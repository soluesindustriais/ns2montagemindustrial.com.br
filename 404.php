<?
$h1         = 'Erro 404: Página não encontrada';
$title      = 'Página não encontrada';
$desc       = 'Navegue pelo menu do nosso site e encontre o que está procurando, escolha abaixo a página que deseja visualizar.';
$key        = '';
$var        = 'Página não encontrada';

include('inc/head.php');
include('inc/search-box.php');
include('inc/vetDestaque.php');
?>

</head>

<body>

    <? include('inc/header.php');?>


    <main class="my-5">
        <div class="content my-5">
            <section class="page-404">
                <article class="full page404">
                    <?=$caminho;?>
                    <h1 class="my-5"><?=$h1?></h1>
                    <img class="img404" src="imagens/404-image.svg" alt="Erro 404">
                    <h2 class="my-5">Digite o produto que você está procurando</h2>
                    <div class=" w-100 searchBox">
                        <form class="d-flex justify-content-center mw-75 w-75 mx-auto" action="busca.php" method="get">
                            <input id="searchBox" name="produto" type="text" class="form-control mw-100"
                                style="height:3em" placeholder="Digite aqui o produto que você está procurando">
                            <button type="submit" class="btn btn-primary mw-25" style="font-size: 1.1rem;"><i
                                    class="fas fa-search d-inline mx-1"></i>Buscar</button>
                        </form>
                    </div>
                    <p class="my-5">Navegue pelo site da <?=$nomeSite?> e encontre o que está procurando, escolha abaixo a página que
                        deseja visualizar.</p>
                        <div class="rowButton404">
                            <a rel="nofollow" title="Voltar a página inicial" class="btn btn-primary  mr-3 "
                                href="<?=$url;?>">Voltar a página inicial</a>

                            <a rel="nofollow" title="Ver O Mapa do site" class="btn btn-primary "
                                href="<?=$url;?>mapa-site">Ver o mapa do site</a>
                        </div>

                    <script>
                        var GOOG_FIXURL_LANG = 'pt-BR';
                        var GOOG_FIXURL_SITE = '<?=$url?>'
                    </script>
                    <script src="http://linkhelp.clients.google.com/tbproxy/lh/wm/fixurl.js;
                "></script>

                </article>
            </section>

    <div class="container">
    <div class="row">
         <div class="col-12 mt-4 pt-2">
            <div id="customer-testi" class="owl-carousel owl-theme owl-loaded owl-drag">
               <div class="owl-stage-outer">
                  <div class="owl-stage">
                     <? foreach($vetDestaque as $palavras => $palavra): ?>
                     <div style="height: 280px; max-height: 100%;" class="owl-item overflow-hidden position-relative products-carousel-home">
                        <a href="<?= $trata->trataAcentos(($palavra['palavra'])); ?>">
                           <div class="card customer-testi border-0 text-center position-relative bg-transparent">
                              <img class="w-100 mw-100 position-absolute absolute-top absolute-left" src="imagens/home/destaque/<?= $trata->trataAcentos(($palavra['palavra'])); ?>.jpg" alt="<?=$trata->retiraHifen($palavra['palavra']);?>">
                              <div class="card-body position-relative overflow-hidden bg-transparent">
                                <!--  <p class="d-none text-dark mt-1"><?=$palavra['texto'];?></p> -->
                                 <h3 class="text-primary text-center mt-3"><?=$trata->retiraHifen($palavra['palavra']);?></h3>
                              </div>
                           </div>
                        </a>
                     </div>
                     <? endforeach; ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
    </div>

    
        </div>
    </main>
    </div><!-- .wrapper -->
    <? include('inc/footer.php');?>
</body>

</html>