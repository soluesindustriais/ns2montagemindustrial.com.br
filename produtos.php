<?
$h1         = 'Produtos';
$title      = 'Produtos';
$desc       = 'Encontre os melhor produtos disponíveis, tudo em um lugar só';
$key        = '';
include ("inc/head.php");
?>
<? include( "inc/header.php" ); ?>

<div class="container my-5 py-5">
    <div class="row my-5">
    <?
    foreach($categorias->getCategorias() as $categoria):
        $categoriaSemAcento = $trata->trataAcentos($categoria);
        $categoriaSemHifen =  $trata->retiraHifen($categoria);
    ?>
        <div class="col-md-4 my-3">
            <div class="card overflow-hidden position-relative">
                <figure><img src="imagens/<?= $categoriaSemAcento."/".$categoriaSemAcento."-01.jpg"; ?>"
                        alt="<?= $categoriaSemHifen; ?>" class="card-img-top"></figure>
                <div class="card-body">
                    <h2 class="card-title"><?= $categoriaSemHifen; ?></h2>
                    <p class="card-text">Encontre tudo sobre <?= $categoriaSemHifen; ?>...</p><a
                        href="<?= $categoriaSemAcento."-categoria"; ?>" class="btn btn-primary">Saiba Mais</a>
                </div>
            </div>
        </div>
        <? endforeach; ?>
    </div>
</div>
<? include( "inc/footer.php" ); ?>