        <div class="cd-hero">
    <ul class="cd-hero-slider autoplay">
        <li class="selected">
            <div class="cd-full-width">
                <h2>Manutenção Preventiva</h2>
                <p>A manutenção predial preventiva e corretiva é realizada constantemente para manter total confiabilidade nas estruturas de construções e também para proteger todas as pessoas que circulam tanto às áreas internas quanto externas. </p>
                <a href="<?=$url?>manutencao-preventiva" class="cd-btn">Saiba mais</a>
            </div>

        </li>
        <li>
            <div class="cd-full-width">
                <h2>Empresa de Manutenção</h2>
                <p> Manutenção predial preventiva e corretiva é realizada constantemente para manter total confiabilidade nas estruturas de construções e também para proteger todas as pessoas que circulam tanto às áreas internas quanto externas.</p>
                <a href="<?=$url?>empresa-de-manutencao" class="cd-btn">Saiba mais</a>
            </div>

        </li>
        <li>
            <div class="cd-full-width">
                <h2>Empresa de manutenção industrial</h2>
                <p>A instalação e manutenção elétrica industrial são partes essenciais do funcionamento de uma indústria, pois, como em todo processo há necessidade de energia elétrica, é indispensável...</p>
                <a href="<?=$url?>empresa-de-manutencao-industrial" class="cd-btn">Saiba mais</a>
            </div>

        </li>
    </ul>
    <div class="cd-slider-nav">
        <nav>
            <span class="cd-marker item-1"></span>
            <ul>
                <li class="selected"><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
                <li><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
                <li><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
            </ul>
        </nav>
    </div>

</div>



        <!-- Hero End -->