    <div class="col-12 my-4">
        <h2>Regiões onde atendemos</h2>
    </div>
<div class="col-12">


        <nav>
            <div class="nav nav-tabs nav-justified" role="tablist">
                <a class="nav-item nav-link active" data-toggle="tab" href="#regiao-central">Região Central</a>
                <a class="nav-item nav-link" data-toggle="tab" href="#zona-norte">Zona Norte</a>
                <a class="nav-item nav-link" data-toggle="tab" href="#zona-oeste">Zona Oeste</a>
                <a class="nav-item nav-link" data-toggle="tab" href="#zona-sul">Zona Sul</a>
                <a class="nav-item nav-link" data-toggle="tab" href="#zona-leste">Zona Leste</a>
                <a class="nav-item nav-link" data-toggle="tab" href="#grande-sp">Grande São Paulo</a>
                <a class="nav-item nav-link" data-toggle="tab" href="#litoral-sp">Litoral de São Paulo</a>
            </div>
        </nav>
        <div class="tab-content">
            <div class="tab-pane fade" id="regiao-central">
                <ul>
                    <li class="list-inline-item"><strong>Aclimação</strong></li>
                    <li class="list-inline-item"><strong>Bela Vista</strong></li>
                    <li class="list-inline-item"><strong>Bom Retiro</strong></li>
                    <li class="list-inline-item"><strong>Brás</strong></li>
                    <li class="list-inline-item"><strong>Cambuci</strong></li>
                    <li class="list-inline-item"><strong>Centro</strong></li>
                    <li class="list-inline-item"><strong>Consolação</strong></li>
                    <li class="list-inline-item"><strong>Higienópolis</strong></li>
                    <li class="list-inline-item"><strong>Glicério</strong></li>
                    <li class="list-inline-item"><strong>Liberdade</strong></li>
                    <li class="list-inline-item"><strong>Luz</strong></li>
                    <li class="list-inline-item"><strong>Pari</strong></li>
                    <li class="list-inline-item"><strong>República</strong></li>
                    <li class="list-inline-item"><strong>Santa Cecília</strong></li>
                    <li class="list-inline-item"><strong>Santa Efigênia</strong></li>
                    <li class="list-inline-item"><strong>Sé</strong></li>
                    <li class="list-inline-item"><strong>Vila Buarque</strong></li>
                </ul>
            </div>
            <div class="tab-pane fade" id="zona-norte">
                <ul>
                    <li class="list-inline-item"><strong>Brasilândia</strong></li>
                    <li class="list-inline-item"><strong>Cachoeirinha</strong></li>
                    <li class="list-inline-item"><strong>Casa Verde</strong></li>
                    <li class="list-inline-item"><strong>Imirim</strong></li>
                    <li class="list-inline-item"><strong>Jaçanã</strong></li>
                    <li class="list-inline-item"><strong>Jardim São Paulo</strong></li>
                    <li class="list-inline-item"><strong>Lauzane Paulista</strong></li>
                    <li class="list-inline-item"><strong>Mandaqui</strong></li>
                    <li class="list-inline-item"><strong>Santana</strong></li>
                    <li class="list-inline-item"><strong>Tremembé</strong></li>
                    <li class="list-inline-item"><strong>Tucuruvi</strong></li>
                    <li class="list-inline-item"><strong>Vila Guilherme</strong></li>
                    <li class="list-inline-item"><strong>Vila Gustavo</strong></li>
                    <li class="list-inline-item"><strong>Vila Maria</strong></li>
                    <li class="list-inline-item"><strong>Vila Medeiros</strong></li>
                </ul>
            </div>
            <div class="tab-pane fade" id="zona-oeste">
                <ul>
                    <li class="list-inline-item"><strong>Água Branca</strong></li>
                    <li class="list-inline-item"><strong>Bairro do Limão</strong></li>
                    <li class="list-inline-item"><strong>Barra Funda</strong></li>
                    <li class="list-inline-item"><strong>Alto da Lapa</strong></li>
                    <li class="list-inline-item"><strong>Alto de Pinheiros</strong></li>
                    <li class="list-inline-item"><strong>Butantã</strong></li>
                    <li class="list-inline-item"><strong>Freguesia do Ó</strong></li>
                    <li class="list-inline-item"><strong>Jaguaré</strong></li>
                    <li class="list-inline-item"><strong>Jaraguá</strong></li>
                    <li class="list-inline-item"><strong>Jardim Bonfiglioli</strong></li>
                    <li class="list-inline-item"><strong>Lapa</strong></li>
                    <li class="list-inline-item"><strong>Pacaembú</strong></li>
                    <li class="list-inline-item"><strong>Perdizes</strong></li>
                    <li class="list-inline-item"><strong>Perús</strong></li>
                    <li class="list-inline-item"><strong>Pinheiros</strong></li>
                    <li class="list-inline-item"><strong>Pirituba</strong></li>
                    <li class="list-inline-item"><strong>Raposo Tavares</strong></li>
                    <li class="list-inline-item"><strong>Rio Pequeno</strong></li>
                    <li class="list-inline-item"><strong>São Domingos</strong></li>
                    <li class="list-inline-item"><strong>Sumaré</strong></li>
                    <li class="list-inline-item"><strong>Vila Leopoldina</strong></li>
                    <li class="list-inline-item"><strong>Vila Sonia</strong></li>
                </ul>
            </div>
            <div class="tab-pane fade" id="zona-sul">
                <ul>
                    <li class="list-inline-item"><strong>Aeroporto</strong></li>
                    <li class="list-inline-item"><strong>Água Funda</strong></li>
                    <li class="list-inline-item"><strong>Brooklin</strong></li>
                    <li class="list-inline-item"><strong>Campo Belo</strong></li>
                    <li class="list-inline-item"><strong>Campo Grande</strong></li>
                    <li class="list-inline-item"><strong>Campo Limpo</strong></li>
                    <li class="list-inline-item"><strong>Capão Redondo</strong></li>
                    <li class="list-inline-item"><strong>Cidade Ademar</strong></li>
                    <li class="list-inline-item"><strong>Cidade Dutra</strong></li>
                    <li class="list-inline-item"><strong>Cidade Jardim</strong></li>
                    <li class="list-inline-item"><strong>Grajaú</strong></li>
                    <li class="list-inline-item"><strong>Ibirapuera</strong></li>
                    <li class="list-inline-item"><strong>Interlagos</strong></li>
                    <li class="list-inline-item"><strong>Ipiranga</strong></li>
                    <li class="list-inline-item"><strong>Itaim Bibi</strong></li>
                    <li class="list-inline-item"><strong>Jabaquara</strong></li>
                    <li class="list-inline-item"><strong>Jardim Ângela</strong></li>
                    <li class="list-inline-item"><strong>Jardim América</strong></li>
                    <li class="list-inline-item"><strong>Jardim Europa</strong></li>
                    <li class="list-inline-item"><strong>Jardim Paulista</strong></li>
                    <li class="list-inline-item"><strong>Jardim Paulistano</strong></li>
                    <li class="list-inline-item"><strong>Jardim São Luiz</strong></li>
                    <li class="list-inline-item"><strong>Jardins</strong></li>
                    <li class="list-inline-item"><strong>Jockey Club</strong></li>
                    <li class="list-inline-item"><strong>M'Boi Mirim</strong></li>
                    <li class="list-inline-item"><strong>Moema</strong></li>
                    <li class="list-inline-item"><strong>Morumbi</strong></li>
                    <li class="list-inline-item"><strong>Parelheiros</strong></li>
                    <li class="list-inline-item"><strong>Pedreira</strong></li>
                    <li class="list-inline-item"><strong>Sacomã</strong></li>
                    <li class="list-inline-item"><strong>Santo Amaro</strong></li>
                    <li class="list-inline-item"><strong>Saúde</strong></li>
                    <li class="list-inline-item"><strong>Socorro</strong></li>
                    <li class="list-inline-item"><strong>Vila Andrade</strong></li>
                    <li class="list-inline-item"><strong>Vila Mariana</strong></li>
                </ul>
            </div>
            <div class="tab-pane fade" id="zona-leste">
                <ul>
                    <li class="list-inline-item"><strong>Água Rasa</strong></li>
                    <li class="list-inline-item"><strong>Anália Franco</strong></li>
                    <li class="list-inline-item"><strong>Aricanduva</strong></li>
                    <li class="list-inline-item"><strong>Artur Alvim</strong></li>
                    <li class="list-inline-item"><strong>Belém</strong></li>
                    <li class="list-inline-item"><strong>Cidade Patriarca</strong></li>
                    <li class="list-inline-item"><strong>Cidade Tiradentes</strong></li>
                    <li class="list-inline-item"><strong>Engenheiro Goulart</strong></li>
                    <li class="list-inline-item"><strong>Ermelino Matarazzo</strong></li>
                    <li class="list-inline-item"><strong>Guianazes</strong></li>
                    <li class="list-inline-item"><strong>Itaim Paulista</strong></li>
                    <li class="list-inline-item"><strong>Itaquera</strong></li>
                    <li class="list-inline-item"><strong>Jardim Iguatemi</strong></li>
                    <li class="list-inline-item"><strong>José Bonifácio</strong></li>
                    <li class="list-inline-item"><strong>Moóca</strong></li>
                    <li class="list-inline-item"><strong>Parque do Carmo</strong></li>
                    <li class="list-inline-item"><strong>Parque São Lucas</strong></li>
                    <li class="list-inline-item"><strong>Parque São Rafael</strong></li>
                    <li class="list-inline-item"><strong>Penha</strong></li>
                    <li class="list-inline-item"><strong>Ponte Rasa</strong></li>
                    <li class="list-inline-item"><strong>São Mateus</strong></li>
                    <li class="list-inline-item"><strong>São Miguel Paulista</strong></li>
                    <li class="list-inline-item"><strong>Sapopemba</strong></li>
                    <li class="list-inline-item"><strong>Tatuapé</strong></li>
                    <li class="list-inline-item"><strong>Vila Carrão</strong></li>
                    <li class="list-inline-item"><strong>Vila Curuçá</strong></li>
                    <li class="list-inline-item"><strong>Vila Esperança</strong></li>
                    <li class="list-inline-item"><strong>Vila Formosa</strong></li>
                    <li class="list-inline-item"><strong>Vila Matilde</strong></li>
                    <li class="list-inline-item"><strong>Vila Prudente</strong></li>
                </ul>
            </div>
            <div class="tab-pane fade" id="grande-sp">
                <ul>
                    <li class="list-inline-item"><strong>São Caetano do sul</strong></li>
                    <li class="list-inline-item"><strong>São Bernardo do Campo</strong></li>
                    <li class="list-inline-item"><strong>Santo André</strong></li>
                    <li class="list-inline-item"><strong>Diadema</strong></li>
                    <li class="list-inline-item"><strong>Guarulhos</strong></li>
                    <li class="list-inline-item"><strong>Suzano</strong></li>
                    <li class="list-inline-item"><strong>Ribeirão Pires</strong></li>
                    <li class="list-inline-item"><strong>Mauá</strong></li>
                    <li class="list-inline-item"><strong>Embu</strong></li>
                    <li class="list-inline-item"><strong>Embu Guaçú</strong></li>
                    <li class="list-inline-item"><strong>Embu das Artes</strong></li>
                    <li class="list-inline-item"><strong>Itapecerica da Serra</strong></li>
                    <li class="list-inline-item"><strong>Osasco</strong></li>
                    <li class="list-inline-item"><strong>Barueri</strong></li>
                    <li class="list-inline-item"><strong>Jandira</strong></li>
                    <li class="list-inline-item"><strong>Cotia</strong></li>
                    <li class="list-inline-item"><strong>Itapevi</strong></li>
                    <li class="list-inline-item"><strong>Santana de Parnaíba</strong></li>
                    <li class="list-inline-item"><strong>Caierias</strong></li>
                    <li class="list-inline-item"><strong>Franco da Rocha</strong></li>
                    <li class="list-inline-item"><strong>Taboão da Serra</strong></li>
                    <li class="list-inline-item"><strong>Cajamar</strong></li>
                    <li class="list-inline-item"><strong>Arujá</strong></li>
                    <li class="list-inline-item"><strong>Alphaville</strong></li>
                    <li class="list-inline-item"><strong>Mairiporã</strong></li>
                    <li class="list-inline-item"><strong>ABC</strong></li>
                    <li class="list-inline-item"><strong>ABCD</strong></li>
                </ul>
            </div>
            <div class="tab-pane fade active show" id="litoral-sp">
                <ul>
                    <li class="list-inline-item"><strong>Bertioga</strong></li>
                    <li class="list-inline-item"><strong>Cananéia</strong></li>
                    <li class="list-inline-item"><strong>Caraguatatuba</strong></li>
                    <li class="list-inline-item"><strong>Cubatão</strong></li>
                    <li class="list-inline-item"><strong>Guarujá</strong></li>
                    <li class="list-inline-item"><strong>Ilha Comprida</strong></li>
                    <li class="list-inline-item"><strong>Iguape</strong></li>
                    <li class="list-inline-item"><strong>Ilhabela</strong></li>
                    <li class="list-inline-item"><strong>Itanhaém</strong></li>
                    <li class="list-inline-item"><strong>Mongaguá</strong></li>
                    <li class="list-inline-item"><strong>Riviera de São Lourenço</strong></li>
                    <li class="list-inline-item"><strong>Santos</strong></li>
                    <li class="list-inline-item"><strong>São Vicente</strong></li>
                    <li class="list-inline-item"><strong>Praia Grande</strong></li>
                    <li class="list-inline-item"><strong>Ubatuba</strong></li>
                    <li class="list-inline-item"><strong>São Sebastião</strong></li>
                    <li class="list-inline-item"><strong>Peruíbe</strong></li>
                </ul>
            </div>


        </div>
    </div>
    <div class="col-12 my-4">
        <h2>Estados onde atendemos</h2>
    </div>
    <div class="col-12 mb-5">

        <nav>
            <div class="nav nav-tabs nav-justified" role="tablist">
                <a href="#rj" title="RJ - Rio de Janeiro  " data-toggle="tab" class="current nav-item nav-link active">RJ</a>
                <a href="#mg" title="MG - Minas Gerais  " data-toggle="tab" class="nav-item nav-link">MG</a>
                <a href="#es" title="ES - Espírito Santo  " data-toggle="tab" class="nav-item nav-link">ES</a>
                <a href="#sp" title="SP - Litoral e Interior  " data-toggle="tab" class="nav-item nav-link">SP</a>
                <a href="#pr" title="PR - Paraná  " data-toggle="tab" class="nav-item nav-link">PR</a>
                <a href="#sc" title="SC - Santa Catarina  " data-toggle="tab" class="nav-item nav-link">SC</a>
                <a href="#rs" title="RS - Rio Grande do Sul  " data-toggle="tab" class="nav-item nav-link">RS</a>
                <a href="#pe" title="PE - Pernambuco  " data-toggle="tab" class="nav-item nav-link">PE</a>
                <a href="#ba" title="BA - Bahia  " data-toggle="tab" class="nav-item nav-link">BA</a>
                <a href="#ce" title="CE - Ceará  " data-toggle="tab" class="nav-item nav-link">CE</a>
                <a href="#go" title="GO e DF - Goiás e Distrito Federal  " data-toggle="tab" class="nav-item nav-link">GO
                    e DF</a>
                <a href="#am" title="AM - Amazonas  " data-toggle="tab" class="nav-item nav-link">AM</a>
                <a href="#pa" title="PA - Pará  " data-toggle="tab" class="nav-item nav-link">PA</a>
            </div>
        </nav>
        <div class="tab-content">
            <div class="tab-pane fade active show" id="rj">
                <ul>
                    <li class="list-inline-item"><strong>Rio de Janeiro </strong></li>
                    <li class="list-inline-item"><strong>São Gonçalo</strong></li>
                    <li class="list-inline-item"><strong>Duque de Caxias</strong></li>
                    <li class="list-inline-item"><strong>Nova Iguaçu</strong></li>
                    <li class="list-inline-item"><strong>Niterói</strong></li>
                    <li class="list-inline-item"><strong>Belford Roxo</strong></li>
                    <li class="list-inline-item"><strong>São João de Meriti</strong></li>
                    <li class="list-inline-item"><strong>Campos dos Goytacazes</strong></li>
                    <li class="list-inline-item"><strong>Petrópolis</strong></li>
                    <li class="list-inline-item"><strong>Volta Redonda</strong></li>
                    <li class="list-inline-item"><strong>Magé</strong></li>
                    <li class="list-inline-item"><strong>Itaboraí</strong></li>
                    <li class="list-inline-item"><strong>Mesquita</strong></li>
                    <li class="list-inline-item"><strong>Nova Friburgo</strong></li>
                    <li class="list-inline-item"><strong>Barra Mansa</strong></li>
                    <li class="list-inline-item"><strong>Macaé</strong></li>
                    <li class="list-inline-item"><strong>Cabo Frio</strong></li>
                    <li class="list-inline-item"><strong>Nilópolis</strong></li>
                    <li class="list-inline-item"><strong>Teresópolis</strong></li>
                    <li class="list-inline-item"><strong>Resende</strong></li>
                </ul>
            </div>
            <div class="tab-pane fade" id="mg">
                <ul>
                    <li class="list-inline-item"><strong>Belo Horizonte</strong></li>
                    <li class="list-inline-item"><strong>Uberlândia</strong></li>
                    <li class="list-inline-item"><strong>Contagem</strong></li>
                    <li class="list-inline-item"><strong>Juiz de Fora</strong></li>
                    <li class="list-inline-item"><strong>Betim</strong></li>
                    <li class="list-inline-item"><strong>Montes Claros</strong></li>
                    <li class="list-inline-item"><strong>Ribeirão das Neves</strong></li>
                    <li class="list-inline-item"><strong>Uberaba</strong></li>
                    <li class="list-inline-item"><strong>Governador Valadares</strong></li>
                    <li class="list-inline-item"><strong>Ipatinga</strong></li>
                    <li class="list-inline-item"><strong>Santa Luzia</strong></li>
                    <li class="list-inline-item"><strong>Sete Lagoas</strong></li>
                    <li class="list-inline-item"><strong>Divinópolis</strong></li>
                    <li class="list-inline-item"><strong>Ibirité</strong></li>
                    <li class="list-inline-item"><strong>Poços de Caldas</strong></li>
                    <li class="list-inline-item"><strong>Patos de Minas</strong></li>
                    <li class="list-inline-item"><strong>Teófilo Otoni</strong></li>
                    <li class="list-inline-item"><strong>Sabará</strong></li>
                    <li class="list-inline-item"><strong>Pouso Alegre</strong></li>
                    <li class="list-inline-item"><strong>Barbacena</strong></li>
                    <li class="list-inline-item"><strong>Varginha</strong></li>
                    <li class="list-inline-item"><strong>Conselheiro Lafeiete</strong></li>
                    <li class="list-inline-item"><strong>Araguari</strong></li>
                    <li class="list-inline-item"><strong>Itabira</strong></li>
                    <li class="list-inline-item"><strong>Passos</strong></li>
                </ul>
            </div>
            <div class="tab-pane fade" id="es">
                <ul>
                    <li class="list-inline-item"><strong>Serra</strong></li>
                    <li class="list-inline-item"><strong>Vila Velha</strong></li>
                    <li class="list-inline-item"><strong>Cariacica</strong></li>
                    <li class="list-inline-item"><strong>Vitória</strong></li>
                    <li class="list-inline-item"><strong>Cachoeiro de Itapemirim</strong></li>
                    <li class="list-inline-item"><strong>Linhares</strong></li>
                    <li class="list-inline-item"><strong>São Mateus</strong></li>
                    <li class="list-inline-item"><strong>Colatina</strong></li>
                    <li class="list-inline-item"><strong>Guarapari</strong></li>
                    <li class="list-inline-item"><strong>Aracruz</strong></li>
                    <li class="list-inline-item"><strong>Viana</strong></li>
                    <li class="list-inline-item"><strong>Nova Venécia</strong></li>
                    <li class="list-inline-item"><strong>Barra de São Francisco</strong></li>
                    <li class="list-inline-item"><strong>Santa Maria de Jetibá</strong></li>
                    <li class="list-inline-item"><strong>Castelo</strong></li>
                    <li class="list-inline-item"><strong>Marataízes</strong></li>
                    <li class="list-inline-item"><strong>São Gabriel da Palha</strong></li>
                    <li class="list-inline-item"><strong>Domingos Martins</strong></li>
                    <li class="list-inline-item"><strong>Itapemirim</strong></li>
                    <li class="list-inline-item"><strong>Afonso Cláudio</strong></li>
                    <li class="list-inline-item"><strong>Alegre</strong></li>
                    <li class="list-inline-item"><strong>Baixo Guandu</strong></li>
                    <li class="list-inline-item"><strong>Conceição da Barra</strong></li>
                    <li class="list-inline-item"><strong>Guaçuí</strong></li>
                    <li class="list-inline-item"><strong>Iúna</strong></li>
                    <li class="list-inline-item"><strong>Jaguaré</strong></li>
                    <li class="list-inline-item"><strong>Mimoso do Sul</strong></li>
                    <li class="list-inline-item"><strong>Sooretama</strong></li>
                    <li class="list-inline-item"><strong>Anchieta</strong></li>
                    <li class="list-inline-item"><strong>Pinheiros</strong></li>
                    <li class="list-inline-item"><strong>Pedro Canário</strong></li>
                </ul>
            </div>
            <div class="tab-pane fade" id="sp">
                <ul>
                    <li class="list-inline-item"><strong>Bertioga</strong></li>
                    <li class="list-inline-item"><strong>Caraguatatuba</strong></li>
                    <li class="list-inline-item"><strong>Cubatão</strong></li>
                    <li class="list-inline-item"><strong>Guarujá</strong></li>
                    <li class="list-inline-item"><strong>Ilhabela</strong></li>
                    <li class="list-inline-item"><strong>Itanhaém</strong></li>
                    <li class="list-inline-item"><strong>Mongaguá</strong></li>
                    <li class="list-inline-item"><strong>Riviera de São Lourenço</strong></li>
                    <li class="list-inline-item"><strong>Santos</strong></li>
                    <li class="list-inline-item"><strong>São Vicente</strong></li>
                    <li class="list-inline-item"><strong>Praia Grande</strong></li>
                    <li class="list-inline-item"><strong>Ubatuba</strong></li>
                    <li class="list-inline-item"><strong>São Sebastião</strong></li>
                    <li class="list-inline-item"><strong>Peruíbe</strong></li>
                    <li class="list-inline-item"><strong>São José dos campos</strong></li>
                    <li class="list-inline-item"><strong>Campinas</strong></li>
                    <li class="list-inline-item"><strong>Jundiaí</strong></li>
                    <li class="list-inline-item"><strong>Sorocaba</strong></li>
                    <li class="list-inline-item"><strong>Indaiatuba </strong></li>
                    <li class="list-inline-item"><strong>São José do Rio Preto </strong></li>
                    <li class="list-inline-item"><strong>Itatiba </strong></li>
                    <li class="list-inline-item"><strong>Amparo </strong></li>
                    <li class="list-inline-item"><strong>Barueri </strong></li>
                    <li class="list-inline-item"><strong>Ribeirão Preto</strong></li>
                    <li class="list-inline-item"><strong>Marília </strong></li>
                    <li class="list-inline-item"><strong>Louveira </strong></li>
                    <li class="list-inline-item"><strong>Paulínia </strong></li>
                    <li class="list-inline-item"><strong>Bauru </strong></li>
                    <li class="list-inline-item"><strong>Valinhos </strong></li>
                    <li class="list-inline-item"><strong>Bragança Paulista </strong></li>
                    <li class="list-inline-item"><strong>Araraquara</strong></li>
                    <li class="list-inline-item"><strong>Americana</strong></li>
                    <li class="list-inline-item"><strong>Atibaia </strong></li>
                    <li class="list-inline-item"><strong>Taubaté </strong></li>
                    <li class="list-inline-item"><strong>Araras </strong></li>
                    <li class="list-inline-item"><strong>São Carlos </strong></li>
                    <li class="list-inline-item"><strong>Itupeva </strong></li>
                    <li class="list-inline-item"><strong>Mendonça </strong></li>
                    <li class="list-inline-item"><strong>Itu </strong></li>
                    <li class="list-inline-item"><strong>Vinhedo </strong></li>
                    <li class="list-inline-item"><strong>Marapoama </strong></li>
                    <li class="list-inline-item"><strong>Votuporanga </strong></li>
                    <li class="list-inline-item"><strong>Hortolândia </strong></li>
                    <li class="list-inline-item"><strong>Araçatuba </strong></li>
                    <li class="list-inline-item"><strong>Jaboticabal </strong></li>
                    <li class="list-inline-item"><strong>Sertãozinho</strong></li>
                </ul>
            </div>
            <div class="tab-pane fade" id="pr">
                <ul>
                    <li class="list-inline-item"><strong>Curitiba</strong></li>
                    <li class="list-inline-item"><strong>Londrina</strong></li>
                    <li class="list-inline-item"><strong>Maringá</strong></li>
                    <li class="list-inline-item"><strong>Ponta Grossa</strong></li>
                    <li class="list-inline-item"><strong>Cascavel</strong></li>
                    <li class="list-inline-item"><strong>São José dos Pinhais</strong></li>
                    <li class="list-inline-item"><strong>Foz do Iguaçu</strong></li>
                    <li class="list-inline-item"><strong>Colombo</strong></li>
                    <li class="list-inline-item"><strong>Guarapuava</strong></li>
                    <li class="list-inline-item"><strong>Paranaguá</strong></li>
                    <li class="list-inline-item"><strong>Araucária</strong></li>
                    <li class="list-inline-item"><strong>Toledo</strong></li>
                    <li class="list-inline-item"><strong>Apucarana</strong></li>
                    <li class="list-inline-item"><strong>Pinhais</strong></li>
                    <li class="list-inline-item"><strong>Campo Largo</strong></li>
                    <li class="list-inline-item"><strong>Almirante Tamandaré</strong></li>
                    <li class="list-inline-item"><strong>Umuarama</strong></li>
                    <li class="list-inline-item"><strong>Paranavaí</strong></li>
                    <li class="list-inline-item"><strong>Piraquara</strong></li>
                    <li class="list-inline-item"><strong>Cambé</strong></li>
                    <li class="list-inline-item"><strong>Sarandi</strong></li>
                    <li class="list-inline-item"><strong>Fazenda Rio Grande</strong></li>
                    <li class="list-inline-item"><strong>Paranavaí</strong></li>
                    <li class="list-inline-item"><strong>Francisco Beltrão</strong></li>
                    <li class="list-inline-item"><strong>Pato Branco</strong></li>
                    <li class="list-inline-item"><strong>Cianorte</strong></li>
                    <li class="list-inline-item"><strong>Telêmaco Borba</strong></li>
                    <li class="list-inline-item"><strong>Castro</strong></li>
                    <li class="list-inline-item"><strong>Rolândia</strong></li>
                </ul>
            </div>
            <div class="tab-pane fade" id="sc">
                <ul>
                    <li class="list-inline-item"><strong>Joinville</strong></li>
                    <li class="list-inline-item"><strong>Florianópolis</strong></li>
                    <li class="list-inline-item"><strong>Blumenau</strong></li>
                    <li class="list-inline-item"><strong>Itajaí</strong></li>
                    <li class="list-inline-item"><strong>São José</strong></li>
                    <li class="list-inline-item"><strong>Chapecó</strong></li>
                    <li class="list-inline-item"><strong>Criciúma</strong></li>
                    <li class="list-inline-item"><strong>Jaraguá do sul</strong></li>
                    <li class="list-inline-item"><strong>Lages</strong></li>
                    <li class="list-inline-item"><strong>Palhoça</strong></li>
                    <li class="list-inline-item"><strong>Balneário Camboriú</strong></li>
                    <li class="list-inline-item"><strong>Brusque</strong></li>
                    <li class="list-inline-item"><strong>Tubarão</strong></li>
                    <li class="list-inline-item"><strong>São Bento do Sul</strong></li>
                    <li class="list-inline-item"><strong>Caçador</strong></li>
                    <li class="list-inline-item"><strong>Concórdia</strong></li>
                    <li class="list-inline-item"><strong>Camboriú</strong></li>
                    <li class="list-inline-item"><strong>Navegantes</strong></li>
                    <li class="list-inline-item"><strong>Rio do Sul</strong></li>
                    <li class="list-inline-item"><strong>Araranguá</strong></li>
                    <li class="list-inline-item"><strong>Gaspar</strong></li>
                    <li class="list-inline-item"><strong>Biguaçu</strong></li>
                    <li class="list-inline-item"><strong>Indaial</strong></li>
                    <li class="list-inline-item"><strong>Mafra</strong></li>
                    <li class="list-inline-item"><strong>Canoinhas</strong></li>
                    <li class="list-inline-item"><strong>Itapema</strong></li>
                </ul>
            </div>
            <div class="tab-pane fade" id="rs">
                <ul>
                    <li class="list-inline-item"><strong>Porto Alegre</strong></li>
                    <li class="list-inline-item"><strong>Caxias do Sul</strong></li>
                    <li class="list-inline-item"><strong>Pelotas</strong></li>
                    <li class="list-inline-item"><strong>Canoas</strong></li>
                    <li class="list-inline-item"><strong>Santa Maria</strong></li>
                    <li class="list-inline-item"><strong>Gravataí</strong></li>
                    <li class="list-inline-item"><strong>Viamão</strong></li>
                    <li class="list-inline-item"><strong>Novo Hamburgo</strong></li>
                    <li class="list-inline-item"><strong>São Leopoldo</strong></li>
                    <li class="list-inline-item"><strong>Rio Grande</strong></li>
                    <li class="list-inline-item"><strong>Alvorada</strong></li>
                    <li class="list-inline-item"><strong>Passo Fundo</strong></li>
                    <li class="list-inline-item"><strong>Sapucaia do Sul</strong></li>
                    <li class="list-inline-item"><strong>Uruguaiana</strong></li>
                    <li class="list-inline-item"><strong>Santa Cruz do Sul</strong></li>
                    <li class="list-inline-item"><strong>Cachoeirinha</strong></li>
                    <li class="list-inline-item"><strong>Bagé</strong></li>
                    <li class="list-inline-item"><strong>Bento Gonçalves</strong></li>
                    <li class="list-inline-item"><strong>Erechim</strong></li>
                    <li class="list-inline-item"><strong>Guaíba</strong></li>
                    <li class="list-inline-item"><strong>Cachoeira do Sul</strong></li>
                    <li class="list-inline-item"><strong>Santana do Livramento</strong></li>
                    <li class="list-inline-item"><strong>Esteio</strong></li>
                    <li class="list-inline-item"><strong>Ijuí</strong></li>
                    <li class="list-inline-item"><strong>Alegrete</strong></li>
                </ul>
            </div>
            <div class="tab-pane fade" id="pe">
                <ul>
                    <li class="list-inline-item"><strong>Recife</strong></li>
                    <li class="list-inline-item"><strong>Jaboatão dos Guararapes</strong></li>
                    <li class="list-inline-item"><strong>Olinda</strong></li>
                    <li class="list-inline-item"><strong>Caruaru</strong></li>
                    <li class="list-inline-item"><strong>Petrolina</strong></li>
                    <li class="list-inline-item"><strong>Paulista</strong></li>
                    <li class="list-inline-item"><strong>Cabo de Santo Agostinho</strong></li>
                    <li class="list-inline-item"><strong>Camaragibe</strong></li>
                    <li class="list-inline-item"><strong>Garanhuns</strong></li>
                    <li class="list-inline-item"><strong>Vitória de Santo Antão</strong></li>
                    <li class="list-inline-item"><strong>Igarassu</strong></li>
                    <li class="list-inline-item"><strong>São Lourenço da Mata</strong></li>
                    <li class="list-inline-item"><strong>Abreu e Lima</strong></li>
                    <li class="list-inline-item"><strong>Santa Cruz do Capibaribe</strong></li>
                    <li class="list-inline-item"><strong>Ipojuca</strong></li>
                    <li class="list-inline-item"><strong>Serra Talhada</strong></li>
                    <li class="list-inline-item"><strong>Araripina</strong></li>
                    <li class="list-inline-item"><strong>Gravatá</strong></li>
                    <li class="list-inline-item"><strong>Carpina</strong></li>
                    <li class="list-inline-item"><strong>Goiana</strong></li>
                    <li class="list-inline-item"><strong>Belo Jardim</strong></li>
                    <li class="list-inline-item"><strong>Arcoverde</strong></li>
                    <li class="list-inline-item"><strong>Ouricuri</strong></li>
                    <li class="list-inline-item"><strong>Escada</strong></li>
                    <li class="list-inline-item"><strong>Pesqueira</strong></li>
                    <li class="list-inline-item"><strong>Surubim</strong></li>
                    <li class="list-inline-item"><strong>Palmares</strong></li>
                    <li class="list-inline-item"><strong>Bezerros</strong></li>
                </ul>
            </div>
            <div class="tab-pane fade" id="ba">
                <ul>
                    <li class="list-inline-item"><strong>Salvador</strong></li>
                    <li class="list-inline-item"><strong>Feira de Santana</strong></li>
                    <li class="list-inline-item"><strong>Vitória da Conquista</strong></li>
                    <li class="list-inline-item"><strong>Camaçari</strong></li>
                    <li class="list-inline-item"><strong>Itabuna</strong></li>
                    <li class="list-inline-item"><strong>Juazeiro</strong></li>
                    <li class="list-inline-item"><strong>Lauro de Freitas</strong></li>
                    <li class="list-inline-item"><strong>Ilhéus</strong></li>
                    <li class="list-inline-item"><strong>Jequié</strong></li>
                    <li class="list-inline-item"><strong>Teixeira de Freitas</strong></li>
                    <li class="list-inline-item"><strong>Alagoinhas</strong></li>
                    <li class="list-inline-item"><strong>Barreiras</strong></li>
                    <li class="list-inline-item"><strong>Porto Seguro</strong></li>
                    <li class="list-inline-item"><strong>Simões Filho</strong></li>
                    <li class="list-inline-item"><strong>Paulo Afonso</strong></li>
                    <li class="list-inline-item"><strong>Eunápolis</strong></li>
                    <li class="list-inline-item"><strong>Santo Antônio de Jesus</strong></li>
                    <li class="list-inline-item"><strong>Valença</strong></li>
                    <li class="list-inline-item"><strong>Candeias</strong></li>
                    <li class="list-inline-item"><strong>Guanambi</strong></li>
                    <li class="list-inline-item"><strong>Jacobina</strong></li>
                    <li class="list-inline-item"><strong>Serrinha</strong></li>
                    <li class="list-inline-item"><strong>Senhor do Bonfim</strong></li>
                    <li class="list-inline-item"><strong>Dias d'Ávila</strong></li>
                    <li class="list-inline-item"><strong>Luís Eduardo Magalhães</strong></li>
                    <li class="list-inline-item"><strong>Itapetinga</strong></li>
                    <li class="list-inline-item"><strong>Irecê</strong></li>
                    <li class="list-inline-item"><strong>Campo Formoso</strong></li>
                    <li class="list-inline-item"><strong>Casa Nova</strong></li>
                    <li class="list-inline-item"><strong>Brumado</strong></li>
                    <li class="list-inline-item"><strong>Bom Jesus da Lapa</strong></li>
                    <li class="list-inline-item"><strong>Conceição do Coité</strong></li>
                    <li class="list-inline-item"><strong>Itamaraju</strong></li>
                    <li class="list-inline-item"><strong>Itaberaba</strong></li>
                    <li class="list-inline-item"><strong>Cruz das Almas</strong></li>
                    <li class="list-inline-item"><strong>Ipirá</strong></li>
                    <li class="list-inline-item"><strong>Santo Amaro</strong></li>
                    <li class="list-inline-item"><strong>Euclides da Cunha</strong></li>
                </ul>
            </div>
            <div class="tab-pane fade" id="ce">
                <ul>
                    <li class="list-inline-item"><strong>Fortaleza</strong></li>
                    <li class="list-inline-item"><strong>caucacia</strong></li>
                    <li class="list-inline-item"><strong>Juazeiro do Norte</strong></li>
                    <li class="list-inline-item"><strong>Maracanaú</strong></li>
                    <li class="list-inline-item"><strong>Sobral</strong></li>
                    <li class="list-inline-item"><strong>Crato</strong></li>
                    <li class="list-inline-item"><strong>Itapipoca</strong></li>
                    <li class="list-inline-item"><strong>Maranguape</strong></li>
                    <li class="list-inline-item"><strong>Iguatu</strong></li>
                    <li class="list-inline-item"><strong>Quixadá</strong></li>
                    <li class="list-inline-item"><strong>Canindé</strong></li>
                    <li class="list-inline-item"><strong>Pacajus</strong></li>
                    <li class="list-inline-item"><strong>Crateús</strong></li>
                    <li class="list-inline-item"><strong>Aquiraz</strong></li>
                    <li class="list-inline-item"><strong>Pacatuba</strong></li>
                    <li class="list-inline-item"><strong>Quixeramobim</strong></li>
                </ul>
            </div>
            <div class="tab-pane fade" id="ma">
                <ul>
                    <li class="list-inline-item"><strong>São Luís</strong></li>
                    <li class="list-inline-item"><strong>Imperatriz</strong></li>
                    <li class="list-inline-item"><strong>São José de Ribamar</strong></li>
                    <li class="list-inline-item"><strong>Timon</strong></li>
                    <li class="list-inline-item"><strong>Caxias</strong></li>
                    <li class="list-inline-item"><strong>Codó</strong></li>
                    <li class="list-inline-item"><strong>Paço do Lumiar</strong></li>
                    <li class="list-inline-item"><strong>Açailândia</strong></li>
                    <li class="list-inline-item"><strong>Bacabal</strong></li>
                    <li class="list-inline-item"><strong>Balsas</strong></li>
                    <li class="list-inline-item"><strong>Barra do Corda</strong></li>
                </ul>
            </div>
            <div class="tab-pane fade" id="pi">
                <ul>
                    <li class="list-inline-item"><strong>Teresina</strong></li>
                    <li class="list-inline-item"><strong>São Raimundo Nonato</strong></li>
                    <li class="list-inline-item"><strong>Parnaíba</strong></li>
                    <li class="list-inline-item"><strong>Picos</strong></li>
                    <li class="list-inline-item"><strong>Uruçuí</strong></li>
                    <li class="list-inline-item"><strong>Floriano</strong></li>
                    <li class="list-inline-item"><strong>Piripiri</strong></li>
                    <li class="list-inline-item"><strong>Campo Maior</strong></li>
                </ul>
            </div>
            <div class="tab-pane fade" id="go">
                <ul>
                    <li class="list-inline-item"><strong>Goiânia</strong></li>
                    <li class="list-inline-item"><strong>Aparecida de Goiânia</strong></li>
                    <li class="list-inline-item"><strong>Anápolis</strong></li>
                    <li class="list-inline-item"><strong>Rio Verde</strong></li>
                    <li class="list-inline-item"><strong>Luziânia</strong></li>
                    <li class="list-inline-item"><strong>Águas Lindas de Goiás</strong></li>
                    <li class="list-inline-item"><strong>Valparaíso de Goiás</strong></li>
                    <li class="list-inline-item"><strong>Trindade</strong></li>
                    <li class="list-inline-item"><strong>Formosa</strong></li>
                    <li class="list-inline-item"><strong>Novo Gama</strong></li>
                    <li class="list-inline-item"><strong>Itumbiara</strong></li>
                    <li class="list-inline-item"><strong>Senador Canedo</strong></li>
                    <li class="list-inline-item"><strong>Catalão</strong></li>
                    <li class="list-inline-item"><strong>Jataí</strong></li>
                    <li class="list-inline-item"><strong>Planaltina</strong></li>
                    <li class="list-inline-item"><strong>Caldas Novas</strong></li>
                </ul>
            </div>
            <div class="tab-pane fade" id="ms">
                <ul>
                    <li class="list-inline-item"><strong>Campo Grande</strong></li>
                    <li class="list-inline-item"><strong>Dourados</strong></li>
                    <li class="list-inline-item"><strong>Três Lagoas</strong></li>
                    <li class="list-inline-item"><strong>Corumbá</strong></li>
                    <li class="list-inline-item"><strong>Ponta Porã</strong></li>
                </ul>
            </div>
            <div class="tab-pane fade" id="mt">
                <ul>
                    <li class="list-inline-item"><strong>Cuiabá</strong></li>
                    <li class="list-inline-item"><strong>Várzea Grande</strong></li>
                    <li class="list-inline-item"><strong>Rondonópolis</strong></li>
                    <li class="list-inline-item"><strong>Sinop</strong></li>
                    <li class="list-inline-item"><strong>Tangará da Serra</strong></li>
                    <li class="list-inline-item"><strong>Cáceres</strong></li>
                    <li class="list-inline-item"><strong>Sorriso</strong></li>
                </ul>
            </div>
            <div class="tab-pane fade" id="am">
                <ul>
                    <li class="list-inline-item"><strong>Manaus</strong></li>
                    <li class="list-inline-item"><strong>Parintins</strong></li>
                    <li class="list-inline-item"><strong>Itacoatiara</strong></li>
                    <li class="list-inline-item"><strong>Manacapuru</strong></li>
                    <li class="list-inline-item"><strong>Coari</strong></li>
                    <li class="list-inline-item"><strong>Centro Amazonense</strong></li>
                </ul>
            </div>
            <div class="tab-pane fade" id="pa">
                <ul>
                    <li class="list-inline-item"><strong>Belém</strong></li>
                    <li class="list-inline-item"><strong>Ananindeua</strong></li>
                    <li class="list-inline-item"><strong>Santarém</strong></li>
                    <li class="list-inline-item"><strong>Marabá</strong></li>
                    <li class="list-inline-item"><strong>Castanhal</strong></li>
                    <li class="list-inline-item"><strong>Parauapebas</strong></li>
                    <li class="list-inline-item"><strong>Itaituba</strong></li>
                    <li class="list-inline-item"><strong>Cametá</strong></li>
                    <li class="list-inline-item"><strong>Bragança </strong></li>
                    <li class="list-inline-item"><strong>Abaetetuba</strong></li>
                    <li class="list-inline-item"><strong>Bragança</strong></li>
                    <li class="list-inline-item"><strong>Marituba</strong></li>
                </ul>
            </div>
        </div>
    </div>