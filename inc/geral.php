<?

ini_set('display_errors', 0); error_reporting(E_ERROR | E_WARNING | E_PARSE); //Comentar para Debugar

$dominio = 'https://www.ns2montagemindustrial.com.br/'; //Descomentar quando houver domínio permanente. Colocar exatamente como no exemplo: https://www.empresa.com.br/ (com a barra(/) no final) para gerar os arquivos(htaccess, sitemap e robots) corretamente.

$nomeSite			= 'NS2 Montagem Industrial';
$slogan				= 'Simplesmente o melhor do ramo!';
$dir  = pathinfo($_SERVER['SCRIPT_NAME']);
$host = $_SERVER['HTTP_HOST'];
$http = $_SERVER['REQUEST_SCHEME'];
if ($dir["dirname"] == "/") { $url = $http."://".$host."/";  }
else { $url = $http."://".$host.$dir["dirname"]."/";  }

if ($_SERVER['HTTP_HOST'] != "localhost") $idAnalytics = "G-07T4Z9Z4DP"; // Analytics

$emailContato		= 'growth.solucoesindustriais@gmail.com';
$rua				= 'Rua Pequetita, 179';
$bairro				= 'Vila Olimpia';
$cidade				= 'São Paulo';
$UF					= 'SP';
$cep				= 'CEP: 04552-060';
$latitude			= '-22.546052';
$longitude			= '-48.635514';
$senhaEmailEnvia	= '102030'; // colocar senha do e-mail mkt@dominiodocliente.com.br
$explode			= explode("/", $_SERVER['PHP_SELF']);
$urlPagina 			= end($explode);
$urlPagina	 		= str_replace('.php','',$urlPagina);
$urlPagina 			== "index"? $urlPagina= "" : "";
$urlAnalytics = str_replace("https://www.", '', $url);
$urlAnalytics = str_replace("/", '', $urlAnalytics);

$idCliente = "";
$siteKey = '6Lfc7g8UAAAAAHlnefz4zF82BexhvMJxhzifPirv';
$secretKey = '6Lfc7g8UAAAAAKi8al32HjrmsdwoFoG7eujNOwBI';
$creditos = 'Soluções Industriais'; 

?>

<!-- CONFIGURAÇÃO SCHEMAS -->

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
      "@type": "ListItem",
      "position": 1,
      "item": {
        "@id": "<?=$url?>informacoes",
        "name": "Informações"
      }
    },
    {
      "@type": "ListItem",
      "position": 1,
      "item": {
        "@id": "<?=$url?>produtos",
        "name": "Produtos"
      }
    },
    {
      "@type": "ListItem",
      "position": 2,
      "item": {
        "@id": "<?=$url?>",
        "name": "<?=$nomeSite?>"
      }
    }]
}

  "@context": "https://schema.org",
  "@type": "Organization",
  "address": {
    "@type": "PostalAddress",
    "addressLocality": "Brasil, São Paulo",
    "streetAddress": "Endereço"
  },
  "email": "growth.solucoesindustriais@gmail.com",
  "name": "<?=$nomeSite?>",
  "telephone": "+55 11 4862-1063",
  "location": {
    "@type": "Place",
    "geo": {
      "@type": "GeoCircle",
      "geoMidpoint": {
        "@type": "GeoCoordinates",
        "latitude": "Geo localidade",
        "longitude": "Geo localidade"
      }
    }
  },
  "image": "imagens/logo/logo.png"
}
</script>

<!--  META-TAGS -->
  <? include('inc/keywords.php'); ?>
 	<meta name="csrf-token" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<title><?=$nomeSite.'  -  '.$h1?></title>
	<base href="<?=$url?>">
	<meta name="description" content="<?=$desc?>">
	<meta name="keywords" content="<?=$key?>">
	<meta name="ICBM" content="-GEO-POSITION">
	<meta name="geo.position" content="GEO-POSITION">
	<meta name="geo.placename" content="SÃO PAULO-SP">
	<meta name="geo.region" content="SP-BR">
	<meta name="robots" content="index,follow">
	<meta name="rating" content="General">
	<meta name="revisit-after" content="7 days">
	<link rel="canonical" href="<?=$url.$urlPagina?>">
	<meta name="author" content="<?=$nomeSite?>">	
	<link rel="shortcut icon" href="<?=$url?>/imagens/logo/favicon.png">
	<meta property="og:region" content="Brasil">
	<meta property="og:title" content="<?=$nomeSite.' - '.$h1?>">
	<meta property="og:type" content="article">
	<meta property="og:url" content="<?=$url.$urlPagina?>">
	<meta property="og:description" content="<?=$desc?>">
  <meta property="og:site_name" content="<?=$nomeSite?>">
  <meta name="theme-color" content="#fefeff">



<?

include('inc/vetCategorias.php');
include('inc/classes/trataString.class.php');
include('inc/classes/criarCategoria.class.php');
include('inc/classes/geradores.class.php');



//Caso a variável $dominio tenha algum valor, ele aplicará os métodos da classe
if(isset($dominio))
{
  // Classe com os métodos pra gerar os arquivos .htaccess, sitemap.xml e robots.txt na raiz
  $gerar = new Gerador();
  $gerar->setDominio($dominio);
  $gerar->generateSitemap();
  $gerar->generateHtaccess();
  $gerar->generateRobots();
};

$trata = new Trata();

$categorias = new Categoria();
$categorias->setCategorias($vetCategorias);

//Função para gerar os arquivos *-categoria.php de cada categoria na raiz e gerar os folders de cada categoria na inc/
//Comentar a mesma ao fazer o deploy
$categorias->criarCategoria();
  

//Breadcrumbs
// 1 NIVEL
$caminho  = '<div id="breadcrumb" itemscope itemtype="https://schema.org/Breadcrumb" >';
$caminho .= '<a rel="home" itemprop="url" href="'.$url.'" title="Home">';
$caminho .= '<span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »';
$caminho .= '<strong><span class="page" itemprop="title" > '.$h1.'</span></strong></div>';

// 2 NIVEL
$caminho2 .= '<div class="container mt-5 pt-5 px-0 pb-2">';
$caminho2 .= '<nav aria-label="breadcrumb">';
$caminho2 .= '<ol class="breadcrumb">';
$caminho2 .= '<li rel="home" itemprop="url" class="breadcrumb-item"><a rel="home" itemprop="url" href="'.$url.'" title="Home">Início</a></li>';
$caminho2 .= '<li class="breadcrumb-item active" aria-current="page"><strong>'.$h1.'</strong></li>';
$caminho2 .= '</ol></nav></div>';


// 3 NIVEL
//Método para criar as breadcrumbs relativas às categorias
$categorias->createBreadcrumb();
include( 'inc/breadcrumb.php' ); 

?>
