<!DOCTYPE html>
<html itemscope itemtype="https://schema.org/Thing" lang="pt-br">
<head>
    <link rel="preload" href="<?=$url?>css/style.css" as="style"> <link rel="stylesheet" href="<?=$url?>css/style.css">
    <meta charset="utf-8">
    <script src="<?=$url?>js/jquery-3.4.1.min.js"></script>
    <script src="<?=$url?>js/lazysizes.min.js" async></script>
    <? include('inc/geral.php'); ?>
    

    <!-- <link rel="preconnect" href="https://use.fontawesome.com">
    <link 
        rel="stylesheet" 
        href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" 
        integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous"
    >
    <link 
        rel="preconnect" 
        href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" 
        integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous"
    > -->
    <link rel="preconnect" href="https://fonts.googleapis.com/">
    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5RZCS22');</script>
<!-- End Google Tag Manager -->
</head>
